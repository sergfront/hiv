;(function ($) { $.fn.datepicker.language['uk'] = {
    days: ['неділя', 'понеділок', 'вівторок', 'середа', 'четвер', 'п\'ятница', 'субота'],
    daysShort: ['нд', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],
    daysMin: ['Нд', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    months: ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень','Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'],
    monthsShort: ['Січ', 'Лют', 'Бер', 'Квіт', 'Трав', 'Черв','Лип', 'Серп', 'Вер', 'Жовт', 'Лист', 'Груд'],
    today: 'Сьогодні',
    clear: 'Очистити',
    dateFormat: 'mm/dd/yyyy',
    timeFormat: 'hh:ii aa',
    firstDay: 1
}; 
$.fn.datepicker.language['en'] = {
    daysShort: ['нд', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],
    daysMin: ['Нд', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    months: ['January','February','March','April','May','June','July','August','September','October','November','December'],
    monthsShort: ['Jan', 'Febr', 'March', 'April', 'May', 'June','July', 'Aug', 'Sept', 'Octob', 'Novem', 'Dec'],
    today: 'today',
    clear: 'clear',
    dateFormat: 'mm/dd/yyyy',
    timeFormat: 'hh:ii aa',
    firstDay: 1
};
})(jQuery);